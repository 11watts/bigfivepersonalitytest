import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container, Header, View, DeckSwiper, Card, CardItem, Content, Thumbnail, Text, Title, Left, Right, Button, Body, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Question from './Question';
import { answerQuestion } from '../actions/testActions';

class QuestionPage extends Component {

    render() {
        const { currentQuestion, questions, answers, answerQuestion } = this.props;
        const { number } = questions[currentQuestion];
        const testSize = questions.length;
        const isLast = currentQuestion === testSize - 1;
        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Question: {number} of {testSize}</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={{ flex: 1 }}>
                        <Question
                            question={questions[currentQuestion]}
                            value={answers[currentQuestion] | 0}
                            answerQuestion={answerQuestion} />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', padding: 8 }}>
                        <View style={{ flex: 1, padding: 8 }}>
                            <Button
                                iconLeft
                                block
                                onPress={() => this.changeQuestion(false)}
                                disabled={currentQuestion === 0}>
                                <Icon name='arrow-back' />
                                <Text>Back</Text>
                            </Button>
                        </View>
                        <View style={{ flex: 1, padding: 8 }}>
                            {
                                isLast ?
                                    <Button
                                        iconRight
                                        primary
                                        block
                                        onPress={() => {
                                            Actions.score({ questions, answers });
                                        }}
                                        disabled={!answers[currentQuestion]}>
                                        <Text>Score Test</Text>
                                        <Icon name='arrow-forward' />
                                    </Button>
                                    :
                                    <Button
                                        iconRight
                                        primary
                                        block
                                        onPress={() => this.changeQuestion(true)}
                                        disabled={!answers[currentQuestion]}>
                                        <Text>Next</Text>
                                        <Icon name='arrow-forward' />
                                    </Button>
                            }
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }

    changeQuestion(next) {
        if (next) {
            Actions.questionPage({ currentQuestion: this.props.currentQuestion + 1 });
        }
        else {
            Actions.pop();
        }
    }

};

const mapStateToProps = state => {
    return {
        questions: state.big5test.questions,
        answers: state.big5test.answers,
    };
}

export default connect(mapStateToProps, { answerQuestion })(QuestionPage);
