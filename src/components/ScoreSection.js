import React, { Component } from 'react';
import { Animated } from 'react-native';
import PropTypes from 'prop-types';
import { Card, CardItem, Container, Text, Radio, View, Body, Icon, ListItem, List, Left, Right, Content } from 'native-base';

const ScoreSection = (props) => {
    const { domain, score, barWidth, maxScore } = props;
    const width = Math.round(100 * score / maxScore);

    return (
        <View style={{ flex: 1, paddingBottom: 12 }}>
            <Text style={{ paddingBottom: 4 }}>{domain}: {score}/{maxScore}</Text>
            <Animated.View style={{
                borderWidth: 1,
                backgroundColor: '#e36',
                padding: 8,
                width: barWidth.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0%', `${width}%`],
                }),
            }}>
            </Animated.View>
        </View>
    );
  };

ScoreSection.propTypes = {
    domain: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    maxScore: PropTypes.number.isRequired,
};

export default ScoreSection;