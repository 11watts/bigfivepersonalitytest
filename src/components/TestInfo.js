import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Linking } from 'react-native';
import { Container, Header, Body, Button, Left, Right, Content, Text, Title, H2 } from 'native-base';



class TestInfo extends Component {

    render() {
        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Test Information</Title>
                    </Body>
                </Header>
                <Content>
                    <Container style={{ padding: 16, alignItems: 'center', height: '100%' }}>
                        <H2>Big Five Personality Test</H2>
                        <Text>
                            This test is a test of big five personality traits created using questions from the International Personality Item Pool.
                            For more information see the{' '}
                            <Text 
                                style={{color: '#0000EE', textDecorationLine: 'underline'}}
                                onPress={() => {
                                    Linking.openURL('https://ipip.ori.org/');
                            }}>
                                International Personality Item Pool website.
                            </Text>
                        </Text>
                        <Button
                            style={{ marginTop: 16 }}
                            onPress={this.startTest}
                            block
                            primary><Text> Start Test </Text></Button>
                    </Container>
                </Content>
            </Container>
        );
    }

    startTest() {
        Actions.questionPage({ currentQuestion: 0 });
    }

};

export default TestInfo;
