import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Animated, Easing } from 'react-native';
import { Container, Header, DeckSwiper, Card, CardItem, Content, Thumbnail, Text, Title, Left, Right, Button, Body, Icon, View } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { resetTest } from '../actions/testActions';
import ScoreSection from './ScoreSection';

const mapResponse = { 5: 1, 4: 2, 3: 3, 2: 4, 1: 5 };

class Score extends Component {

    state = {
        barWidth: new Animated.Value(0),
    }

    componentDidMount() {
        Animated.timing(this.state.barWidth, {
            toValue: 1,
            duration: 2000,
            easing: Easing.linear,
        }).start();
    }

    render() {
        const { barWidth } = this.state;
        const { questions, answers, resetTest } = this.props;
        const numOfQuestion = this.calculateRange(questions);
        const score = this.scoreTest({ questions, answers });

        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Your Results</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={{ flex: 1, padding: 8 }}>
                        {
                            Object.keys(score).map((domain, i) =>
                                <ScoreSection
                                    domain={domain}
                                    score={score[domain]}
                                    barWidth={barWidth}
                                    maxScore={numOfQuestion[domain] * 5}
                                    key={i} />)
                        }
                        <Button block
                            onPress={() => {
                                resetTest();
                                Actions.testInfo();
                            }}>
                            <Text>Restart Test</Text>
                        </Button>
                    </View>
                </Content>
            </Container >
        );
    }

    calculateRange(questions) {
        const numOfQuestion = {
            Openness: 0,
            Conscientiousness: 0,
            Extraversion: 0,
            Agreeableness: 0,
            Neuroticism: 0,
        }
        for (q of questions) {
            const { domain } = q;
            numOfQuestion[domain]++;
        }

        return numOfQuestion;
    }

    scoreTest({ questions, answers }) {
        const score = {
            Openness: 0,
            Conscientiousness: 0,
            Extraversion: 0,
            Agreeableness: 0,
            Neuroticism: 0,
        }

        for (i in questions) {
            const { domain, keyedPlus } = questions[i];
            //const v = mapResponse[v];
            score[domain] += keyedPlus ? answers[i] : mapResponse[answers[i]];
        }

        return score;
    }
};

export default connect(null, { resetTest })(Score);
