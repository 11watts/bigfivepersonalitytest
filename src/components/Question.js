import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardItem, Container, Text, Radio, View, Body, Icon, ListItem, List, Left, Right, Content } from 'native-base';

const answers = [
    {
        text: 'Strongly Disagree',
        value: 1,
    },
    {
        text: 'Somewhat disagree',
        value: 2,
    },
    {
        text: 'Neutral',
        value: 3,
    },
    {
        text: 'Somewhat agree',
        value: 4,
    },
    {
        text: 'Strongly agree',
        value: 5,
    },
];

const Question = (props) => {
    const { text, number, domain } = props.question;
    const { value, answerQuestion } = props;

    return (
        <Container style={{ flex: 1, height: '100%' }}>
            <View style={{ alignItems: 'center', paddingTop: 8}}>
                <Text>{text}</Text>
            </View>
            <List>
                {answers.map((a, index) =>
                    <ListItem
                        onPress={() => answerQuestion(props.question.number - 1, a.value)}
                        selected={a.value === value}
                        key={index}>
                        <Left>
                            <Text>{a.text}</Text>
                        </Left>
                        <Right>
                            <Radio onPress={() => answerQuestion(props.question.number - 1, a.value)} selected={a.value === value} />
                        </Right>
                    </ListItem>
                )}
            </List>
        </Container>
    );
};

Question.propTypes = {
  value: PropTypes.number.isRequired,
  answerQuestion: PropTypes.func.isRequired,
  question: PropTypes.shape({
    text: PropTypes.string.isRequired,
    number: PropTypes.number.isRequired,
    domain: PropTypes.string.isRequired,
  }),
};

export default Question;
