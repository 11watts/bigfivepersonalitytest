import data from '../data/Big5Questions.json';
import { ANSWER_QUESTION, RESET_TEST } from '../actions/types';

const initState = {
    questions: data,
    answers: {},
};

export default (state = initState, action) => {
    switch (action.type) {
        case ANSWER_QUESTION:
            const { number, value } = action;
            const answers = { ...state.answers, [number]: value }
            return { ...state, answers };
        case RESET_TEST:
            return initState;
        default:
            return state;
    }
}
