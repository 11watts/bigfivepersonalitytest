import { combineReducers } from 'redux';
import big5Reducer from './big5Reducer';

export default combineReducers({
    big5test: big5Reducer,
});