import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Scene, Router } from 'react-native-router-flux';
import QuestionPage from './components/QuestionPage';
import TestInfo from './components/TestInfo';
import Score from './components/Score';

const RouterComponent = () => {
    return (
        <Router hideNavBar="true">
            <Scene key="root" hideNavBar>
                <Scene
                    key="testInfo"
                    component={TestInfo}
                    title="Big 5 Test Info "
                    initial />
                <Scene
                    key="questionPage"
                    panHandlers={null}
                    component={QuestionPage}
                />
                <Scene
                    key="score"
                    panHandlers={null}
                    component={Score}
                />
            </Scene>
        </Router>
    );
};

export default RouterComponent;
