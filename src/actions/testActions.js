import { ANSWER_QUESTION, RESET_TEST } from './types';

export const answerQuestion = (number, value) => {
    return {
        type: ANSWER_QUESTION,
        number,
        value,
    }
};

export const resetTest = () => {
    return {
        type: RESET_TEST,
    }
};
