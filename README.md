Big Five Personality Test
======

![gif](https://bitbucket.org/11watts/bigfivepersonalitytest/raw/090424283611013f4d792bb2f489cbf622c7952b/sample/demo.gif).

A big five personality test based on questions from the International Personality Item Pool. An android build can be downloaded [here](https://bitbucket.org/11watts/bigfivepersonalitytest/raw/090424283611013f4d792bb2f489cbf622c7952b/sample/app.apk).